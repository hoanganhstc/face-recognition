import cv2, sys, numpy, os
import dlib
import threading
import numpy as np
import time

size = 2 #number using to down scale frame size
(im_width, im_height) = (112, 112)
predict_quality = 250

color_yel = (0,255,255)
color_red = (0,0,255)
line_width = 3

found_object = False
fps_detection = 0
fps_preview = 0
limit_fps_preview = 100.0
is_running = True

fn_dir = '../../database'
fn_model = '../../model/model.yml'
fn_save = '../../image_result/'

try:
    type_input = sys.argv[1]
    type_name = sys.argv[2]
except:
    print("Wrong parameter, example:")
    print("argv[1]: 'image', 'video', 'camera'")
    print("argv[2]: filename if choise image or video\n         0,1,2...if using camera")
    sys.exit(0)

#Use fisherRecognizer on camera stream
detector = dlib.get_frontal_face_detector()

#load model after train data
model = cv2.face.FisherFaceRecognizer_create()
#model = cv2.face.EigenFaceRecognizer_create()
model.read(fn_model)

# Create a list of images and a list of corresponding names
(images, lables, names, id) = ([], [], {}, 0)

# Get the folders containing the training data
for (subdirs, dirs, files) in os.walk(fn_dir):
    # Loop through each folder named after the subject in the photos
    for subdir in dirs:
        names[id] = subdir
        subjectpath = os.path.join(fn_dir, subdir)
        # Loop through each photo in the folder
        for filename in os.listdir(subjectpath):
            # Skip non-image formates
            f_name, f_extension = os.path.splitext(filename)
            if(f_extension.lower() not in
                    ['.png','.jpg','.jpeg','.gif','.pgm']):
                print("Skipping "+filename+", wrong file type")
                continue
            path = subjectpath + '/' + filename
            lable = id
            # Add to training data
            images.append(cv2.imread(path, 0))
            lables.append(int(lable))
        id += 1

# initial first frame and output
if type_input == "image":
    frame=cv2.imread(type_name)
    frame_output = frame
else:
    if type_input == "video":
        cap = cv2.VideoCapture(type_name)
    elif type_input == "camera":
        cap = cv2.VideoCapture(int(type_name)) 
    w = cap.get(3)
    h = cap.get(4)
    frame = np.zeros((int(h),int(w),3), np.uint8)
    frame_output = np.zeros((int(h),int(w),3), np.uint8)

class Thread_capture(threading.Thread):
    def run(self):
        global frame
        global frame_output
        global is_running
        
        while is_running == True:
            if not type_input == "image": #if using camera or video file
                (ret, frame) = cap.read()
                frame_output = frame
                
                if not ret:
                    is_running = False
                    
                time.sleep(1.0/limit_fps_preview)
    
class Thread_detect(threading.Thread):
    def run(self):
        global frame
        global found_object
        global frame_output
        global fps_detection
        
        frame_counter = 0

        while is_running == True:
            #measure fps
            if frame_counter == 0:
                time_start = time.time()
            frame_counter +=1
            
            rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            dets = detector(rgb_image)
            
            for det in dets:                          
                # get face and predict it
                face = frame[det.top():det.bottom(), det.left():det.right()]
                face_resize = cv2.resize(face, (im_width, im_height))
                face_gray = cv2.cvtColor(face_resize, cv2.COLOR_BGR2GRAY)
                
                # Try to recognize the face
                prediction = model.predict(face_gray)
                
                if prediction[1]<predict_quality:
                    color = (0,255,0)
                    text = '%s - %.0f' % (names[prediction[0]],prediction[1])
                else:
                    color = (0,0,255)
                    text = '%s - %.0f' % ('unknow',prediction[1])
                    print text
                    
                # draw infor to frame output
                cv2.rectangle(frame_output,(det.left(), det.top()), (det.right(), det.bottom())
                            , color, line_width)
                cv2.putText(frame_output, text,(det.left(), det.top()), cv2.FONT_HERSHEY_PLAIN,1, color)
                
            # display fps
            time_elapsed = time.time() - time_start
            if time_elapsed >= 1.0:
                fps_detection = frame_counter
                frame_counter=0
                
            if type_input == "image":
                cv2.imwrite(fn_save + "result.jpg", frame_output)
                break
            
class Thread_preview(threading.Thread):
    def run(self):
        global fps_preview
        global is_running
        
        frame_counter = 0
        
        while is_running == True:
            #measure fps
            if frame_counter == 0:
                time_start = time.time()
            frame_counter +=1
            
            # Show the image and check for ESC being pressed
            cv2.imshow('recognition frame', frame_output)
            
            # create timeout to fixed fps
            if cv2.waitKey(int(1000/limit_fps_preview)) == 27:
                is_running = False
                
            # display fps
            time_elapsed = time.time() - time_start
            if time_elapsed >= 1.0:
                fps_preview = frame_counter
                frame_counter=0
                print("###################################")
                print("FPS detection: "+str(fps_detection))
                print("FPS preview: "+str(fps_preview))

if __name__ == "__main__":
    print("Started capture thread")
    task_capture = Thread_capture()
    task_capture.start()
    
    print("Started detection thread")
    task_detect = Thread_detect()
    task_detect.start()
    
    print("Started preview thread")
    task_preview = Thread_preview()
    task_preview.start()