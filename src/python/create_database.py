
import cv2, sys, numpy, os
import dlib

fn_dir = '../../database'

start_collect = False
    
try:
    type_input = sys.argv[1]
    type_name = sys.argv[2]
    fn_name = sys.argv[3]
except:
    print("Wrong parameter, example:")
    print("argv[1]: 'video', 'camera'")
    print("argv[2]: filename if choise video\n         0,1,2...if using camera")
    print("argv[3]: ID you want to apply to face, maybe name...")
    sys.exit(0)    

#Create path for database
path = os.path.join(fn_dir, fn_name)
if not os.path.isdir(path):
    os.mkdir(path)
(im_width, im_height) = (112, 112)

#load haar
detector = dlib.get_frontal_face_detector()
color_red = (0,0,255)
color_green = (0,255,0)
line_width = 3

#check input is camera or video file
if type_input == "video":
    cap = cv2.VideoCapture(type_name)
elif type_input == "camera":
    cap = cv2.VideoCapture(int(type_name))
    
width = cap.get(3)
height = cap.get(4)

print str(width) + "x" + str(height)

# Generate name for image file
pin=sorted([int(n[:n.find('.')]) for n in os.listdir(path)
     if n[0]!='.' ]+[0])[-1] + 1
     
print("Collecting data sample")     

count = 0
pause = 0
if type_input == "video":
    count_max = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
else:
    count_max = 0

while True:
    # Put the image from the webcam into 'frame'
    (ret, frame) = cap.read()
    if not ret:
        print("Can't get frame. Exit...")
        sys.exit()
    
    # start detect face
    cv2.putText(frame, "Press SPACE to collecting data", (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,255,0),2)

    rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    dets = detector(rgb_image)
    for det in dets:
        cv2.putText(frame, fn_name, (det.left() - 10, det.top() - 10), cv2.FONT_HERSHEY_PLAIN, 1,(0, 255, 0))   
        
        if start_collect == False:
            cv2.rectangle(frame,(det.left(), det.top()), (det.right(), det.bottom()), color_green, line_width)
        else:
            cv2.rectangle(frame,(det.left(), det.top()), (det.right(), det.bottom()), color_red, line_width)

            # To create diversity, only save every fith detected image
            if(pause == 0):
                print("Saving training sample "+str(count+1)+"/"+str(count_max))
                face = frame[det.top():det.bottom()
                            , det.left():det.right()]
                face_resize = cv2.resize(face, (im_width, im_height))
                # Save image file
                cv2.imwrite('%s/%s.png' % (path, pin), face_resize)

                pin += 1
                count += 1
                pause = 1

            if(pause > 0):
                pause = (pause + 1) % 5
            
    cv2.imshow('Data sample', frame)
    
    key = cv2.waitKey(1)
    if key == 27:
        break
    elif key == 32:
        start_collect = not start_collect
