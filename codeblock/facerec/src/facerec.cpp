#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <vector>
#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"

using namespace cv;
using namespace cv::face;
using namespace std;

static int size = 2;
static int im_width = 112;
static int im_height = 92;
static int data_max_size = 500; //max number person will processing
static double predict_confidence_dst = 150.0;
const string fn_haar  = "../../../../haar/haarcascade_frontalface_default.xml";
const string fn_dir   = "../../../../database/"; //skip to train project
const string fn_model = "../../../../model/model_cpp.yml";
const string file_config_name = fn_dir+"config.cfg";


//Simple struct to return from lsfiles
struct List {
	vector<string> files;
	vector<string> folders;
};
//All of the hard work
struct List lsfiles(string folder)  //(c) http://stackoverflow.com/a/20847429/1009816
{
    vector<string> files; //Will be added to List
	vector<string> folders; //Will be added to List
    char search_path[200];
    sprintf(search_path, "%s*.*", folder.c_str());
    WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(search_path, &fd);
    if(hFind != INVALID_HANDLE_VALUE)
    {
        do
        {
            // read all (real) files in current folder, delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
            {
                files.push_back(fd.cFileName);
            } else //Put folders into vector
			{
				folders.push_back(fd.cFileName);
			}
        }while(::FindNextFile(hFind, &fd));
        ::FindClose(hFind);
    }
	List me;
	me.files = files;
	me.folders = folders;

    return me;
}

void load_config(string file_name)
{
    //get app config from file confic in database folder
    cout<<"Loading config from: "+file_name<<endl;
    std::ifstream file_config(file_name);
    if (file_config.fail())
    {
        cout<<"Failed to open config.cfg in database folder"<<endl<<"Using default config!"<<endl;
    }
    std::string line;
    int num_line = 0;
    while (getline(file_config, line)) {
        num_line++;
        if(num_line==1)
        {
            im_width = stoi(line);
            cout<<"im_width face: "<<im_width<<endl;
        }
        else if(num_line==2)
        {
            im_height=stoi(line);
            cout<<"im_height face: "<<im_height<<endl;
        }
        else if(num_line==3)
        {
            size=stoi(line);
            cout<<"size down scaled: "<<size<<endl;
        }
    }

	file_config.close();
}

int main(int argc, char *argv[])
{
    string input_type;
    string input_id;
    int id[data_max_size];
    string names[data_max_size];
    int total_person = 0;

    if (argc>2)
    {
        input_type = argv[1];
        input_id = argv[2];
    }
    else
    {
        cout<<endl<<"ERROR:Must have 3 agrument from cmd"<<endl;
        cout<<"argv[1]: choise input: image, video, camera"<<endl;
        cout<<"argv[2]: 'file name' if input is image, video, ID 0,1,2,..if input is camera"<<endl;
        return 1;
    }

    //init haar
    CascadeClassifier haar_cascade;
    haar_cascade.load(fn_haar);

    //load config
    load_config(file_config_name);
    //load model, face recognition
    cout <<"Load fn_dir face from: " <<fn_dir<<endl;
    cout <<"Load haarcascade face from: " <<fn_haar<<endl;
    cout <<"Load model after train from: " <<fn_model<<endl<<endl;

    //start search all image each person on database folder
	List you = lsfiles(fn_dir); //Get contents of directory
	vector<string>::iterator folders_begin = you.folders.begin();
	vector<string>::iterator folders_end = you.folders.end();

	//get all name of person
	for(; folders_begin != folders_end; folders_begin++){
        if(*folders_begin != ".." && *folders_begin != ".")
        {
            //cout << "[D] " << *folders_begin << "\n";
            names[total_person] = std::string(*folders_begin);
            id[total_person] = total_person;
            total_person++;
        }
	}
    cout<<"total: "<<total_person<<" ID from "<<fn_dir<<endl;
    for(int i=0;i<total_person; i++)
    {
        cout<<names[i]<<endl;
    }

    Ptr<FisherFaceRecognizer> model_read = FisherFaceRecognizer::create();
    model_read->read(fn_model);

    VideoCapture cap_cam(atoi(input_id.c_str()));
    VideoCapture cap_vid(input_id);

    Mat frame;
    while(1)
    {
        //get frame or image
        if (input_type == "image")
        {
            frame = imread(input_id);
        }
        else if (input_type == "video")
        {
            cap_vid >> frame; //video object
        }
        else
        {
            cap_cam >> frame; //camera object
        }

        Mat original = frame.clone();
        Mat gray;
        Mat gray_resized;
        cvtColor(original, gray, COLOR_BGR2GRAY);
        resize(gray, gray_resized, Size(gray.cols/size, gray.rows/size));
        vector< Rect_<int> > faces;
        haar_cascade.detectMultiScale(gray_resized, faces);
        for(size_t i = 0; i < faces.size(); i++) {
            //get location of rectangle contain face then draw it
            Rect face_rect = cv::Rect(faces[i].x*size, faces[i].y*size, faces[i].width*size,faces[i].height*size);
            rectangle(original, face_rect, Scalar(0, 255,0), 1);

            //using fisher, predict person ID from trained data
            Mat face = gray_resized(faces[i]);
            Mat face_resized;
            cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
            //int prediction = model_read->predict(face_resized);
            int predict_label;
            double predict_confidence;
            model_read->predict(face_resized, predict_label, predict_confidence);
            cout<<"Confidence: "<<predict_confidence<<endl;
            if (predict_confidence<predict_confidence_dst)
            {
                //draw person's info to frame
                string name_person = names[predict_label];
                string str_confidence = to_string(predict_confidence);
                putText(original, name_person+"-"+str_confidence, Point(faces[i].x*size - 10, faces[i].y*size - 10), FONT_HERSHEY_PLAIN, 2.0, Scalar(0,255,0), 1);
            }
            else
            {
                putText(original, "Unknow", Point(faces[i].x*size - 10, faces[i].y*size - 10), FONT_HERSHEY_PLAIN, 2.0, Scalar(0,0,255), 1);
            }
        }

        //show image
        imshow("face_recognizer", original);

        //timeout for display
        if (input_type == "image")
        {
            char key = (char) waitKey(0);
            if(key == 27)
                break;
        }
        else
        {
            char key = (char) waitKey(1);
            if(key == 27)
                break;
        }
    }
	return 0;
}
