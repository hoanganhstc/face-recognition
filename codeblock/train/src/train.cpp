#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <vector>
#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"
#include <fstream>
#include <sstream>
#include <map>

using namespace cv;
using namespace cv::face;
using namespace std;

static int size = 2;
static int im_width = 112;
static int im_height = 92;
static int data_max_size = 500; //max number person in database
const string fn_haar  = "../../../../haar/haarcascade_frontalface_default.xml";
const string fn_dir   = "../../../../database/"; //skip to train project
const string fn_model = "../../../../model/model_cpp.yml";
const string file_config_name = fn_dir+"config.cfg";

//Simple struct to return from lsfiles
struct List {
	vector<string> files;
	vector<string> folders;
};
//All of the hard work
struct List lsfiles(string folder)  //(c) http://stackoverflow.com/a/20847429/1009816
{
    vector<string> files; //Will be added to List
	vector<string> folders; //Will be added to List
    char search_path[200];
    sprintf(search_path, "%s*.*", folder.c_str());
    WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(search_path, &fd);
    if(hFind != INVALID_HANDLE_VALUE)
    {
        do
        {
            // read all (real) files in current folder, delete '!' read other 2 default folder . and ..
            if(! (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
            {
                files.push_back(fd.cFileName);
            } else //Put folders into vector
			{
				folders.push_back(fd.cFileName);
			}
        }while(::FindNextFile(hFind, &fd));
        ::FindClose(hFind);
    }
	List me;
	me.files = files;
	me.folders = folders;

    return me;
}

void load_config(string file_name)
{
    //get app config from file confic in database folder
    cout<<"Loading config from: "+file_name<<endl;
    std::ifstream file_config(file_name);
    if (file_config.fail())
    {
        cout<<"Failed to open config.cfg in database folder"<<endl<<"Using default config!"<<endl;
    }
    std::string line;
    int num_line = 0;
    while (getline(file_config, line)) {
        num_line++;
        if(num_line==1)
        {
            im_width = stoi(line);
            cout<<"im_width face: "<<im_width<<endl;
        }
        else if(num_line==2)
        {
            im_height=stoi(line);
            cout<<"im_height face: "<<im_height<<endl;
        }
        else if(num_line==3)
        {
            size=stoi(line);
            cout<<"size down scaled: "<<size<<endl;
        }
    }

	file_config.close();
}

int main(int argc, char *argv[])
{
    int id[data_max_size];
    string names[data_max_size];
    int total_person = 0;
    int total_image = 0;
    vector<Mat> v_images;
    vector<int> v_labels;

    //start search all image each person on database folder
	List you = lsfiles(fn_dir); //Get contents of directory
	vector<string>::iterator folders_begin = you.folders.begin();
	vector<string>::iterator folders_end = you.folders.end();

	load_config(file_config_name);

	cout<<"Scanning: "<<fn_dir<<endl;

	//Step1: counting total person in database = number of subdirectory
	for(; folders_begin != folders_end; folders_begin++){
        if(*folders_begin != ".." && *folders_begin != ".")
        {
            //cout << "[D] " << *folders_begin << "\n";
            names[total_person] = std::string(*folders_begin);
            id[total_person] = total_person;
            total_person++;
        }
	}
    cout<<"total: "<<total_person<<" ID from database"<<endl;
    for(int i=0;i<total_person; i++)
    {
        cout<<names[i]<<endl;
    }

    //go to all subdir
	for(int i=0;i<total_person; i++)
    {
        string subdir = fn_dir+names[i]+"/";
        cout<<"Entering: "<<subdir<<endl;

        List sub_you = lsfiles(subdir);
        vector<string>::iterator files_begin = sub_you.files.begin();
        vector<string>::iterator files_end = sub_you.files.end();

        //only using file
        for(; files_begin != files_end; files_begin++)
        {
            //cout << "[F] " << *files_begin << "\n";
            string image_path = subdir+std::string(*files_begin);

            //check correct image file name
            int pos_extension_image = -1;
            pos_extension_image = image_path.find(".png");
            if (pos_extension_image == -1)
            {
                pos_extension_image = image_path.find(".jpg");
            }
            if (pos_extension_image == -1)
            {
                pos_extension_image = image_path.find(".jpeg");
            }

            //if correct image name
            if(pos_extension_image >= 0)
            {
                //resized image on database if frame size of image != frame size has been train
                cv::Mat image_temp = cv::imread(image_path, 0);
                if(image_temp.cols != im_width ||image_temp.rows!=im_height)
                {
                    cv::Mat image_resized;
                    try
                    {
                        cv::resize(image_temp, image_resized, Size(im_width, im_height));
                    }
                    catch(...)
                    {
                        cout<<"\nERROR!!!\nImage damaged: "+image_path+"\nPlease remove this file before training"<<endl<<"Exit!!"<<endl;
                        return 1;
                    }
                    cv::imwrite(image_path, image_resized);
                    v_images.push_back(image_resized); //read gray image
                }
                else
                {
                    v_images.push_back(image_temp);
                }

                    v_labels.push_back(id[i]);
                    cout << image_path <<";"<<v_labels.at(total_image)<< "\n";
                    total_image++;
            }
            else
            {
                cout<<"Skipping: "<<image_path<<",  Only acept image with .png, .jpg"<<endl;
            }
        }
        cout<<endl;
    }
    cout<<"Total: "<<v_images.size()<<" image sample for "<<total_person<<" person ID "<<endl;
    cout<<"Please wait until training process done"<<endl;

    Ptr<FisherFaceRecognizer> model = FisherFaceRecognizer::create();
    model->train(v_images, v_labels);
    model->save(fn_model);
    cout << "Trained, model stored in: " << fn_model << endl;

	return 0;
}
