#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <dir.h>
#include <vector>
#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"

using namespace cv;
using namespace cv::face;
using namespace std;

static int size = 2;
static int im_width = 112;
static int im_height = 92;
const string fn_haar  = "../../../../haar/haarcascade_frontalface_default.xml";
const string fn_dir   = "../../../../database/"; //skip to train project
const string file_config_name = fn_dir+"config.cfg";

void load_config(string file_name)
{
    //get app config from file confic in database folder
    cout<<"Loading config from: "+file_name<<endl;
    std::ifstream file_config(file_name);
    if (file_config.fail())
    {
        cout<<"Failed to open config.cfg in database folder"<<endl<<"Using default config!"<<endl;
    }
    std::string line;
    int num_line = 0;
    while (getline(file_config, line)) {
        num_line++;
        if(num_line==1)
        {
            im_width = stoi(line);
            cout<<"im_width face: "<<im_width<<endl;
        }
        else if(num_line==2)
        {
            im_height=stoi(line);
            cout<<"im_height face: "<<im_height<<endl;
        }
        else if(num_line==3)
        {
            size=stoi(line);
            cout<<"size down scaled: "<<size<<endl;
        }
    }

	file_config.close();
}

int main(int argc, char** argv)
{
    string input_type;
    string input_id;
    string name_person;
    int total_face = 0;

    if (argc < 4)
    {
        cout<<endl<<"ERROR:Must have 4 agrument from cmd"<<endl;
        cout<<"argv[1]: choise input: video, camera"<<endl;
        cout<<"argv[2]: 'file name' if input is video. ID 0,1,2,..if input is camera"<<endl;
        cout<<"argv[3]: name person will add to database"<<endl;
        return 1;
    }
    else
    {
        input_type = argv[1];
        input_id = argv[2];
        name_person = argv[3];
    }

    mkdir((fn_dir+name_person+"/").c_str());

    load_config(file_config_name);
    //load model, face recognition
    cout <<"Will save database face to: " <<fn_dir<<endl;
    cout <<"Load haarcascade face from: " <<fn_haar<<endl;
    cout <<"Person name: " <<name_person<<endl;

    //init haar
    CascadeClassifier haar_cascade;
    haar_cascade.load(fn_haar);

    VideoCapture cap_cam(atoi(input_id.c_str()));
    VideoCapture cap_vid(input_id);

    Mat frame;
    while(1)
    {
        if (input_type == "video")
        {
            cap_vid >> frame; //video object
        }
        else
        {
            cap_cam >> frame; //camera object
        }

        Mat original = frame.clone();
        Mat gray;
        Mat gray_resized;
        cvtColor(original, gray, COLOR_BGR2GRAY);
        resize(gray, gray_resized, Size(gray.cols/size, gray.rows/size));
        vector< Rect_<int> > faces;
        haar_cascade.detectMultiScale(gray_resized, faces);

        for(size_t i = 0; i < faces.size(); i++)
        {
            total_face++;
            //get location of rectangle contain face
            Rect face_rect = cv::Rect(faces[i].x*size, faces[i].y*size, faces[i].width*size,faces[i].height*size);

            //resize face to store in database
            Mat face = gray_resized(faces[i]);
            Mat face_resized;
            string file_path = fn_dir+name_person+"/"+to_string(total_face)+".png";
            cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
            cv::imwrite(file_path, face_resized);
            cout<<"Stored: "<<file_path<<endl;

            //write person's info to frame
            rectangle(original, face_rect, Scalar(0, 255,0), 1);
            putText(original, name_person, Point(faces[i].x*size-10, faces[i].y*size-10), FONT_HERSHEY_PLAIN, 2.0, Scalar(0,255,0), 1);
        }

        //show image
        imshow("face_recognizer", original);

        char key = (char) waitKey(1);
        if(key == 27)
            break;
    }
    return 0;
}
